# dwm

- Not gaps
- No Systray
- Use something like sxhkd for keybindings & keep config.h clean.
- Diff files of applied patches are saved in folder named "patched-diffs".
  - The file patched-diffs/patches even lists the order in which these patches were applied & also lists other patches of interest to me, which i may use sometime in future, in Sha Allah.
